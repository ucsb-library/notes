<header>
<time datetime="2022-01-10">10 January 2022</time>

#  <small>Bundling Javascript assets with</small> Deno in Rails  #

This note explores the possibility of using Deno for Javascript
  bundling in Rails environments, particularly Rails engines, as
  demonstrated by my work in [samvera/hyrax#5314].

[samvera/hyrax#5314]: <https://github.com/samvera/hyrax/pull/5314>

</header>
<section id="problem">

##  The Problem  ##

The “default” gem for the asset pipeline in Rails, Sprockets, is built
  upon Babel 5, which hasn’t seen a new release since
  [<time datetime="2016-01-20">January of 2016</time>][Babel v5.8.35].
<span class="PARENTHETICAL">(Babel itself is, at time of writing,
  currently on version 7.)</span>
This means that <strong class="MARK">the past 6 years of developments
  in the Javascript language are not available</strong> to Rails
  applications using Sprockets.

This is not a problem for most Rails *applications* because better
  solutions exist.
In Rails 6, the common solution was to use Webpacker, and in Rails 7,
  you can set up your Rails application with your choice of bundler via
  the gem `jsbundling-rails`.
New Rails applications which need “contemporary” (post‐Ecmascript 2016)
  Javascripts typically use one of these solutions, and consequently
  <strong class="MARK">Sprockets‐based packages have largely fallen out
  of maintenance</strong>.
This includes some testing frameworks that older (Rails 5 era)
  applications may have been designed for, like [jasmine-rails].

This creates an issue for Rails *engines*, because
  <strong class="MARK">none of these alternate bundling mechanisms are
  “engine‐aware”.</strong>
As a result, the recommended way of including contemporary Javascripts
  in a Rails *engine* is to ① publish those JavaScripts as an N·P·M
  package, and ② tell applications to require that package on their
  end, either using N·P·M / Yarn (in the bundling scenario) or via
  importmaps and a Javascript C·D·N like Skypack (Rails 7).
<strong class="MARK">This solution is not really a solution at
  all;</strong> it just delegates the problem of handling Javascript
  code to Node and provides mechanisms for then pulling the result back
  into the frontend of your application.
It is also unsatisfactory, to me, for the following reasons :—

01. Even in the simplest case, where a Rails engine is simply using
      GitHub as its package registry and not publishing to N·P·M
      proper, it <strong class="MARK">requires creating and maintaining
      a `package.json` file</strong> and adds complexity to the release
      process.

02. <strong class="MARK">Rails applications *also* will be required to
      participate in the N·P·M ecosystem</strong> and use supporting
      Javascript bundling mechanisms, which could potentially create
      difficult work for existing applications and increase the cost of
      migration.
    The ideal situation for a Rails engine is one which “just works”
      with minimal configuration by applications, and one which can
      work side‐by‐side with existing code.

03. The Javascripts which accompany a Rails engine aren’t intended to
      be used independently from a corresponding application, so
      <strong class="MARK">publishing them separately seems
      strange</strong>.

04. Depending on N·P·M for part of the build process
      <strong class="MARK">encourages participation in the N·P·M
      ecosystem</strong>, which may be undesirable for more reasons
      than I have time to get into here.

Since any N·P·M solution is essentially pushing the problem of managing
  Javascript components off onto a different program *anyway*, I
  wondered <strong class="MARK">if it mightn’t be possible to be a bit
  more judicious in the selection of that program</strong>, and to use
  [Deno] for this task instead of Node.
I managed to create an exploratory implementation for the open‐source
  software [Hyrax], which this note explores.

<aside>

**Why should I care about supporting the latest Javascript in my Rails
  engine?**

There are a number of syntactic features added to Javascript in recent
  years that younger developers will expect to have access to.
I would say private class fields (`this.#foo`) and the null‐coalescing
  operator (`a ?? b`) are the two biggest additions to the language
  that Sprockets doesn’t seem to support, and I can tell you that *no*
  Javascript developer who has grown comfortable using these features
  will want to go back to a world where they aren’t allowed.

</aside>

[Babel v5.8.35]: <https://github.com/babel/babel/releases/tag/v5.8.35>
[jasmine-rails]: <https://github.com/searls/jasmine-rails>
[Deno]: <https://deno.land/>
[Hyrax]: <https://hyrax.samvera.org/>

</section>
<section id="about-deno">

##  What Is Deno?  ##

[Deno] is a modern Javascript runtime which ships as a single
  executable with no dependencies.
It is rooted in web standards and aims to minimize its own extensions,
  such that <strong class="MARK">code which runs in Deno runs pretty
  much anywhere</strong>.
Frontend Javascript code which is developed in Deno should be able to
  run in the browser with no modifications, unlike Node code which
  often requires (at the very least) CommonJS to Ecmascript module
  bundling or transpilation.

<strong class="MARK">Deno also supports Typescript</strong>, as well as
  typechecked Javascript using [JSDoc] type annotations.
I personally think all frontend code should be written in vanilla
  Javascript, not Typescript, but have found the process of adding type
  annotations to my Javascript code to not be particularly onerous.
In any case, this functionality is easy to turn on or off.

Despite shipping as a single executable, <strong class="MARK">Deno
  comes with a number of useful features which are much more difficult
  to acquire and maintain when using Node</strong> :—

 +  `deno test` provides <strong class="MARK">code testing
      capabilities</strong>.

 +  `deno fmt` <strong class="MARK">reformats files to conform to
      standard conventions</strong>, and `deno lint`
      <strong class="MARK">lints them for basic errors</strong>.

 +  `deno bundle` <strong class="MARK">packages a Javascript module
      into a single file</strong>.

These commands give us everything we need to package and test
  Javascripts for our engine.

<aside>

**Is it really okay to require Rails application developers to install Deno to
  build and test their application?**

This question is really “is it okay to require Rails application
  developers to install *something which can’t be installed with
  `bundle install`*” since every Rails engine has a long list of
  dependencies otherwise.
I am sympathetic to this concern, but am not sure that limiting
  ourselves to only those JavaScript frameworks with easy `bundle`
  solutions is useful, especially since most of those frameworks seem
  to have left Rails engine development behind.

I personally don’t see requiring Deno for Javascript to be that unlike
  requiring ImageMagick or ffmpeg for audiovisual assets; some things
  just require a library which is beyond what Rails can provide.

</aside>

<div role="note">

**A note on Deno compatibility:**
At present Deno is a dynamically‐linked binary which requires `glibc`.
This is a problem for Linux distributions which use `musl` instead,
  like Alpine Linux.
Your solutions to this are essentially as follows :—

 +  Provide glibc compatibility through something like
      [alpine-pkg-glibc].

 +  Run the `deno bundle` step on a different machine and copy the
      result into the correct location prior to serving assets.

See [denoland/deno#3711] for more. There is a [denoland/deno:alpine]
  Docker container if you want to see what Deno and Alpine Linux look
  like from a Docker perspective.

[alpine-pkg-glibc]: <https://github.com/sgerrand/alpine-pkg-glibc>
[denoland/deno#3711]: <https://github.com/denoland/deno/issues/3711>
[denoland/deno:alpine]: <https://hub.docker.com/r/denoland/deno>

</div>

[JSDoc]: <https://jsdoc.app/>

</section>
<section id="doing-it">

##  Making It Work  ##

For Hyrax, <strong class="MARK">it was important that our existing
  Sprockets‐based Javascript bundling continue to work</strong>, for
  the following reasons :—

01. Some of our dependencies depend on Sprockets.

02. Existing applications *definitely* depend on Sprockets.

03. We have a lot of existing Javascript code, and it would be a pain
      to try to migrate it all at once.

So, it was important that whatever new Deno solution I came up with
  <strong class="MARK">did not interfere with the existing asset
  pipeline</strong> in any way.
To keep the two separate, I decided to put new JavaScript assets in one
  of two new folders, `app/scripts/` and `lib/scripts/`, with existing
  assets residing in the classic `app/assets/javascripts/`.
I created a new file at `app/scripts/hyrax/mod.js`, which will be used
  as the entrypoint for bundling.

<div role="note">

`mod.js` is the conventional entrypoint name for Javascript files in
  Deno, and stands for “module”.
Another important file is `deps.js`, which collects module
  dependencies.
<span class="PARENTHETICAL">(These files are not treated differently
  from any other Javascript file, and are merely useful
  conventions.)</span>

</div>

The next task was to “install” Deno to `bin/deno`.
The following file (or something similar) needs to be copied to
  `bin/deno` during application generation.
<strong class="MARK">It’s helpful to have a rake task to make this easy
  for existing applications to do as well</strong>; I called ours
  `hyrax:deno:install` :—

<pre><code><small>#!/usr/bin/env ruby</small>
APP_ROOT = <strong>File</strong>.<em>expand_path</em>('..', <em>__dir__</em>)
<strong>Dir</strong>.<em>chdir</em>(APP_ROOT) <strong>do</strong>
  <strong>begin</strong>
    <em>exec</em> "deno", *ARGV
  <strong>rescue</strong> <strong>Errno</strong>::ENOENT
    $stderr.<em>puts</em> "Deno executable was not detected in the system."
    $stderr.<em>puts</em> "Download Deno at &lt;https://github.com/denoland/deno_install&gt;."
    <em>exit</em> 1
  <strong>end</strong>
<strong>end</strong></code></pre>

One can then use `system 'bin/deno', exception: true` within Rake tasks
  to perform various Deno commands.
<strong class="MARK">The bundling script I wrote looks something like
  this</strong> :—

<pre><code><em>namespace</em> :hyrax <strong>do</strong>
  <em>namespace</em> :deno <strong>do</strong>
    <em>desc</em> "Bundle Deno scripts"
    <em>task</em> bundle: :environment <strong>do</strong>
    <em>task</em> :bundle, [:reload] => :environment <strong>do</strong> |_t, args|

      <small># Make the `public/scripts` directory if it isn’t present in the application.</small>
      <strong>Dir</strong>.<em>mkdir</em>(<strong>Rails</strong>.<em>root</em>.<em>join</em>('public', 'scripts')) <strong>unless</strong> <strong>File</strong>.<em>exist?</em>(<strong>Rails</strong>.<em>root</em>.<em>join</em>('public', 'scripts'))

      <small># Get the path to our engine, and then `app/scripts/hyrax/mod.js`.</small>
      hyrax_path = <strong>Gem</strong>.<em>loaded_specs</em>['hyrax'].<em>full_gem_path</em>
      deno_hyrax = <strong>File</strong>.<em>join</em>(hyrax_path, 'app', 'scripts', 'hyrax', 'mod.js').<em>to_s</em>

      <small># The output location: `public/scripts/hyrax.js`.</small>
      deno_hyrax_bundle = <strong>Rails</strong>.<em>root</em>.<em>join</em>('public', 'scripts', 'hyrax.js').<em>to_s</em>

      <small># Bundle.</small>
      <small># If called as `rake hyrax:deno:bundle[reload]`, will reload cached dependencies.</small>
      <strong>if</strong> args.reload && !%w[0 no false].<em>include?</em>(args.reload)
        <em>system</em> 'bin/deno', 'bundle', '-r', '--no-check=remote', deno_hyrax, deno_hyrax_bundle, exception: true
      <strong>else</strong>
        <em>system</em> 'bin/deno', 'bundle', '--no-check=remote', deno_hyrax, deno_hyrax_bundle, exception: true
      <strong>end</strong>
      <em>puts</em> 'Hyrax Javascript assets bundled to public/scripts/hyrax.js.'
    <strong>end</strong>
  <strong>end</strong>
<strong>end</strong></code></pre>

You’ll need to run this task every time your engine (but not
  application) Javascripts change, so it might be a good idea to make
  it a dependency of `assets:precompile`.
The final step is to <strong class="MARK">actually *load* the bundled
  Javascripts on pages rendered by the engine</strong>.
This is a matter of changing tags which look like this :—

<pre><code><small>&lt;!-- application js --&gt;</small>
&lt;%= <em>javascript_include_tag</em> 'application' %&gt;</code></pre>

—: to ones which look like this :—

<pre><code><small>&lt;!-- engine js --&gt;</small>
&lt;%= <em>javascript_include_tag</em> '/scripts/hyrax', type: 'module' %&gt;

<small>&lt;!-- application js --&gt;</small>
&lt;%= <em>javascript_include_tag</em> 'application', defer: '' %&gt;</code></pre>

You will note that the <strong class="MARK">application Javascripts are
  still included using the same mechanisms they always have
  been</strong> (plus a `defer` tag).
**Using Deno to build engine Javascripts consequently
  <strong class="MARK">has *no* impact on application Javascript
  development or bundling</strong>.**
Indeed, <strong class="MARK">applications would have to define their
  own Rake tasks</strong> if they want to take advantage of Deno for
  their own development purposes.

<aside>

**Why the `defer`?**

As far as loading and execution is concerned, H·T·M·L has three major
  classes of scripts :— classic scripts, modules, and asynchronous
  scripts (which can be classic or module).
Classic scripts are normally downloaded and run the second they are
  encountered by the parser, while module scripts run after the D·O·M
  has finished loading, just before the `DOMContentLoaded` event fires.
The `defer` attribute makes classic scripts behave like module scripts
  in this manner, and ensures that the (classic) application Javascript
  runs *after* the (module) engine Javascript, instead of before.
(The order of the tags is important here, too.)

This enables the engine Javascript to define properties on the global
  object which application Javascripts can then directly use.
This is the *preferred* mechanism for application~engine Javascript
  interactions; in the Hyrax case, applications can interact with the
  engine frontend via the global `Hyrax` object defined by
  `/public/scripts/hyrax.js`.

</aside>

</section>
<section id="js-design-notes">

##  Notes on Engine JavaScript Design  ##

Although the above is *most* of what one needs to get Deno working in
  Rails, the fact that one will ultimately be loading engine and
  application Javascripts from separate files
  <strong class="MARK">introduces a <span class="PARENTHETICAL">(I
  would argue, good)</span> boundary between application and engine
  frontend code</strong> which it takes some conscious design to
  manage.
As one example, the Hyrax engine has a number of initializers which are
  meant to be run on page load.
I wanted to make it easy for applications to expand the set of
  initializers, so I defined `Hyrax` as a class with a static
  `intializers` property.
This property is an object with string or symbol keys, so applications
  can actually override existing initializers if they need to as
  well :—

<pre><code><strong>class</strong> <strong>Hyrax</strong> {
  <strong>constructor</strong> () {
    <strong>for</strong> (<strong>const</strong> <em>name</em> <strong>of</strong> <strong>Reflect</strong>.<em>ownKeys</em>(<strong>Hyrax</strong>.<em>initializers</em>)) {
      <small>// `Reflect.ownKeys` gets symbols in addition to names.</small>
      <strong>Hyrax</strong>.<em>initializers</em>[<em>name</em>].<em>call</em>(<strong>this</strong>);
    }
  }

  <strong>static</strong> <em>initializers</em> = {};
}

<small>// Assigning `Hyrax` to `globalThis` makes it accessible in
// applications without needing to use an import or require. (In a
// browser environment, `globalThis` is more popularly known as
// `window`.)</small>
<em>globalThis</em>.<strong>Hyrax</strong> = <strong>Hyrax</strong>;</code></pre>

Next, I set up a callback to freeze the set of initializers, and
  initialize a `Hyrax` instance to the `hyrax` global object, on page
  load :—

<pre><code>{
  <strong>const</strong> <em>onLoad</em> = () => {
    <strong>Object</strong>.<em>freeze</em>(<strong>Hyrax</strong>.<em>initializers</em>);
    <strong>Object</strong>.<em>defineProperty</em>(<strong>Hyrax</strong>, "initializers", {
      configurable: <strong>false</strong>,
      writable: <strong>false</strong>,
    });
    <em>globalThis</em>.<em>hyrax</em> = <strong>new</strong> <strong>Hyrax</strong>();
    <em>document</em>.<em>removeEventListener</em>("DOMContentLoaded", <em>onLoad</em>);
  };
  <em>document</em>.<em>addEventListener</em>("DOMContentLoaded", <em>onLoad</em>);
}</code></pre>

Because the `"DOMContentLoaded"` event fires *after* both module
  scripts and classic scripts with the `defer` attribute, applications
  have an opportunity to add or modify the `Hyrax.initializers` object
  as much as they like before the `hyrax` instance is created.
But once every script has had a chance to execute, the `hyrax` instance
  is created and the set of initializers is frozen in time.

This is actually how *all* of the Hyrax engine initializers are loaded
  right now, since none of them have been migrated over from Sprockets
  into Deno.

</section>
