<header>
<time datetime="2022-02-22">22 February 2022</time>

#  M·3 &amp; Valkyrie  #

This note explores reading [Houndstooth][samvera/houndstooth]‐style
  [machine·readible metadata modelling][samvera/M3WG] (M·3) to set up
  [Valkyrie][samvera/valkyrie] objects for use in, and beyond,
  { [Samvera Hyrax][samvera/hyrax]
  | [Surfliner Comet][surfliner/comet] }.

</header>
<section id="overview">

##  Overview  ##

[Valkyrie][samvera/valkyrie] is a data mapper library for metadata,
  written in Ruby.
It enables applications (including, but not necessarily limited to,
  Rails apps) to handle metadata on objects in a consistent way,
  without worrying about how that metadata is persisted (e·g to a
  database).
[Surfliner Comet][surfliner/comet], and increasingly
  [Samvera Hyrax][samvera/hyrax] (which Comet is built on), use
  Valkyrie for internal (in‐application) representation of their
  metadata models.

At present, <strong class="MARK">models in Hyrax (and Comet) are loaded
  into Valkyrie from a Y·A·M·L file</strong> in the `config/metadata`
  directory.
This metadata file forms a very minimal representation of the
  information Hyrax needs to run.
It looks like this :—

<figure>

<pre><code><em>attributes</em>:
  <em>title</em>:
    <em>type</em>: string
    <em>multiple</em>: <strong>true</strong>
    <em>index_keys</em>:
      - "title_sim"
      - "title_tesim"
    <em>form</em>:
      <em>required</em>: <strong>true</strong>
      <em>primary</em>: <strong>true</strong>
      <em>multiple</em>: <strong>false</strong></code></pre>

<figcaption>

A Y·A·M·L file describing a *required* `title` field which can take
  multiple values, is indexed with the keys `title_sim` and
  `title_tesim`, and has form options
  `{ required: true, primary: true, multiple: false }`.

</figcaption>
</figure>

This configuration is sufficient for applications so long as their
  metadata remains “selfcontained”.
However, <strong class="MARK">a problem arises when the same metadata
  (and thus Valkyrie configuration) is shared between apps, or needs to
  cross an A·P·I boundary</strong>.
In Project Surfliner, we are developing an application called
  [Superskunk][surfliner/superskunk] to serve as Comet’s A·P·I.
But how is Superskunk to know what a Comet `title` means?
And how does it know how to map that `title` to, say,
  [Dublin Core Metadata Elements][DCES], when an A·P·I consumer
  requests metadata appropriate for [O·A·I‐P·M·H][OAI/PMH]?
We need a machine·readable way of sharing this information.

And <strong class="MARK">as it happens, these questions fall neatly
  inline with those considered by the Samvera
  [Machine‐readable Metadata Modeling Specification (M3) Working Group][samvera/M3WG]
  in 2019–2020</strong>.
The final product of this working group, the
  [Houndstooth M3 Metadata Model][samvera/houndstooth/m3.yml], conveys
  not only property identity (via the `property_uri` property), but
  also mapping information of the sort useful to A·P·I applications
  like Superskunk.

The process of loading Y·A·M·L files into Valkyrie via Comet is
  currently managed by a small `surfliner_schema` gem, providing both a
  “native” `SurflinerSchema::Reader` interface and a
  `SurflinerSchema::HyraxLoader` interface for compatibility with Hyrax
  methods.
The hope is that this gem will be used in Superskunk as well.
This document explores the feasibility of having this gem read in M·3
  metadata files directly, and using `SurflinerSchema::HyraxLoader` to
  generate the necessary Hyrax simple schemas on·the·fly.

</section>
<section id="datatypes">

##  Bridging Valkyrie and M·3 Datatypes  ##

<strong class="MARK">The biggest point of divergence between Valkyrie
  and M·3 is probably their handling of datatype information.</strong>
In M·3, properties have a `data_type` property, which is an ordinary
  R·D·F datatype.
In Valkyrie, however, attribute types are drawn from the following set
  of `Dry::Types`, which correspond to datatypes available in Ruby :—

<ul class="PARAGRAPH">
  <li><code>RDF::URI</code> (≈ <code>xsd:anyURI</code>)</li>
  <li><code>String</code> (≈ <code>xsd:string</code>)</li>
  <li><code>Integer</code> (≈ <code>xsd:long</code>)</li>
  <li><code>Float</code> (≈ <code>xsd:double</code>)</li>
  <li><code>DateTime</code> (≈ <code>xsd:dateTime</code>)</li>
  <li><code>Boolean</code> (≈ <code>xsd:boolean</code>)</li>
  <li><code>RDF::Literal</code> (any RDF type)</li>
  <li><code>Valkyrie::ID</code> (for internal use)</li>
</ul>

In an ideal world, Valkyrie would probably use `RDF::Literal`s for all
  metadata attributes.
However, <strong class="MARK">existing code largely does <em>not</em>
  follow this practice.</strong>
**A question remains as to what extent using `Dry::Types` like
  `DateTime` in place of R·D·F types like `xsd:dateTime` is
  acceptable.**

  + Using `Dry::Types` <strong class="MARK">probably requires some
      level of datatype validation</strong> (an R·D·F literal of
      `"rootbeer"^^xsd:float` cannot be represented as a `Float` in
      Ruby).
    **Do we need to support data which does not validate against its
      datatype?**

  + Similarly, using `Dry::Types` <strong class="MARK">will result in a
      loss of the original lexical representation</strong> for the
      metadata.
    For example, an `xsd:long` might have a leading zero or plus sign,
      which would be lost on conversion to `Dry::Types::Integer`.
    **Is it important that these be preserved?**
    (If it is, further research may be required on how we handle these
      values in Comet.)

  + The `Dry::Types` datatype most closely corresponding to many X·S·D
      types <strong class="MARK">does not exactly match the set of
      valid X·S·D values.</strong>
    We *could* represent `xsd:nonNegativeInteger` as a
      `Dry::Types::Integer`, for example, even though
      `Dry::Types::Integer` can be both positive and negative.
    This might result in metadata being stored which is outside of the
      bounds of the model.
    **Is this a problem?**

In short, **how much of an effort is necessary to make Valkyrie∕Hyrax
  datatyping conform to R·D·F and X·S·D Datatypes?**

###  Cardinality  ###

Closely‐related to questions of datatyping are questions of
  cardinality.
In Valkyrie, cardinality is stored *as* datatype information, with the
  following distinctions present in current code :—

<ul class="PARAGRAPH">
  <li><strong class="MARK">Singular</strong> (cardinality = 1)</li>
  <li><strong class="MARK">Optional</strong> (cardinality = [0,1])</li>
  <li><strong class="MARK">Multiple</strong> (cardinality = [0,∞))</li>
</ul>

M·3, on the other hand, allows arbitrary minimum and maximum
  cardinality specification, distinct from questions of datatype.
**Do we need to support cardinalities beyond those listed above?**
And **to what extent do these cardinalities need to be enforced?**

</section>
<section id="builtins">

##  Application Builtins 🆚 Schema Definitions  ##

Hyrax (and thus Comet) makes use of a number of metadata properties
  internally to do its work.
Because they are defined in the application, they
  <strong class="MARK">may not be present in the M·3 model</strong>.
Even worse, properties defined in the M·3 model
  <strong class="MARK"><em>may conflict</em> with those used by Hyrax
  internally</strong>.
This raises the following questions :—

  + **To what extent is it “safe” to make use of internal metadata
      properties?**
    **How important is it that every metadata field be represented in
      M·3?**

  + If the M·3 model and Hyrax make use of the same metadata property
      with different definitions (for example, different expectations
      about what constitutes a `title`), **how should this be
      mitigated?**

  + **Should we be looking into deriving internal application metadata
      from properties specified in the M·3 model or vice·versa?**

</section>
<section id="questions">

##  Other Open Questions  ##

###  I·R·I’s for Mappings  ###

Right now mappings are defined in M·3 as follows :—

<pre><code><em>mappings</em>:
  <em>dpla</em>:
    <em>name</em>: Digital Public Library of America
  <em>dc</em>:
    <em>name</em>: Dublin Core</code></pre>

<strong class="MARK">This approach is not quite desirable for machine
  processing</strong> because terms like `dc` or `Dublin Core` are not
  necessarily weldefined.
Does “Dublin Core” refer to [Dublin Core Metadata Elements][DCES] or
  [DCMI Metadata Terms][DCMI]?
**It would be best if mappings also specified an `iri` property which
  mapping applications could use for mapping identification.**

###  Application‐specific Metadata  ###

The Hyrax simple schema includes form options for metadata, which
  controls how metadata fields should be presented to users inputting
  data.
Should the field be required?
Should it take multiple values?

While it may be possible to derive this metadata from M·3 schema
  information, <strong class="MARK">it might also be beneficial to have
  a dedicated place in M·3 for application‐specific metadata</strong>
  (ideally namespaced to an I·R·I).
Alternatively, there is a chance the `mapping` property could be used
  for this purpose (a property with a `display_mode` mapping of
  `example:rainbow` should be displayed in rainbow colours, for
  example) [this would undoubtedly be a hack].

###  Indexing  ###

The Hyrax simple schema includes an `index_keys` property for keys to
  use when indexing in Solr.
These can probably be derived from the `indexing` property in M·3; for
  example a `symbol` string property would have the suffix `_tsim`.
Still, <strong class="MARK">it’s unclear to me how exactly this
  property is intended to be read</strong> and it might be a little
  underspecified.

Many Hyrax properties index as a `_tesim`, which is for specifically
  English text.
There aren’t equivalent suffixes for other languages and I am mildly
  hesitant about this.

</section>
<section id="oos">

##  Out of Scope  ##

###  Schema Versioning & Migrations  ###

M·3 offers a `version` property which could conceivably be used to
  associate different objects with different versions of the same
  schema.
But this is likely to be a lot of work and is out‐of‐scope for now.
So too is migrating between schema versions.

###  Property Documentation  ###

M·3 defines display labels, definitions, and usage guidelines for
  properties.
My guess is that these are actually insufficient, as there is no clear
  path for how to localize them.
But they could conceivably be incorporated into a { Comet ∣ Hyrax }
  U·I.

###  Nondata Values  ###

M·3 allows for the definition of both (in O·W·L terms)
  *object properties* (with `range`) and *data properties* (with
  `data_type`).
This document has dealt primarily with the latter; the question of how
  to represent object properties in Comet is (I think) unresolved.

</section>
<section id="wildernes">

##  Wild Ideas  ##

I don’t want to unbury any skeleton horses, so the remarks in this
  section should be taken as idle curiosities and not requests for
  change.
I’m recording them now only so that they are available for future
  reference.

###  Schemas in K·D·L  ###

Using Y·A·M·L for schema definition has the advantage of being easy to
  modify (especially for nontechnical users), but
  <strong class="MARK">suffers in readability and fragility as file
  sizes get large</strong>, in part due to its significant whitespace.
[K·D·L][KDL] is a new document format which mitigates these concerns,
  and could provide other benefits as well.
Here is a sample property definition in K·D·L :—

<pre><code><strong>property</strong> "title" (<em>irl</em>)"http://purl.org/dc/terms/title" {
  (<em>default</em>)<strong>display_label</strong> <em>lang</em>="en" "DCMI Metadata Terms"
  (<em>default</em>)<strong>definition</strong> <em>lang</em>="en" "A name to aid in identifying a work or collection."
  (<em>collection</em>)<strong>definition</strong> <em>lang</em>="en" "A name to aid in identifying a work or collection."
  <strong>requirement</strong> "required"
  <strong>sample_value</strong> "Roadqueen: Eternal Roadtrip to Love"
  <strong>available_on</strong> {
    <strong>class</strong> "GenericWork"
    <strong>class</strong> "Collection"
  }
  <strong>range</strong> (<em>irl</em>)"http://www.w3.org/2000/01/rdf-schema#Literal"
  <strong>data_type</strong> (<em>irl</em>)"http://www.w3.org/2001/XMLSchema#string"
  <strong>cardinality</strong> <em>minimum</em>=1 <em>maximum</em>=1
  <strong>index_documentation</strong> <em>lang</em>="en" "Title should be indexed as searchable and <strong>displayable."
  <strong>indexing</strong> "stored_searchable" "displayable"
  (<em>dc11</em>)<strong>mapping</strong> "http://purl.org/dc/elements/1.1/title"
  (<em>dcterms</em>)<strong>mapping</strong> "http://purl.org/dc/terms/title"
}</code></pre>

</section>

[DCES]: <https://www.dublincore.org/specifications/dublin-core/dces/>
[DCMI]: <https://www.dublincore.org/specifications/dublin-core/dcmi-terms/>
[KDL]: <https://kdl.dev>
[OAI/PMH]: <http://www.openarchives.org/OAI/openarchivesprotocol.html>
[samvera/M3WG]: <https://samvera.atlassian.net/wiki/spaces/samvera/pages/422320348/Machine-readable+Metadata+Modeling+Specification+M3+Working+Group>
[samvera/houndstooth]: <https://github.com/samvera-labs/houndstooth>
[samvera/houndstooth/m3.yml]: <https://github.com/samvera-labs/houndstooth/blob/017dffc285e78191ae6a3058f2a1bb1903c7d362/m3.yml>
[samvera/hyrax]: <https://github.com/samvera/hyrax>
[samvera/valkyrie]: <https://github.com/samvera/valkyrie>
[surfliner/comet]: <https://gitlab.com/surfliner/surfliner/-/tree/trunk/comet>
[surfliner/superskunk]: <https://gitlab.com/surfliner/surfliner/-/tree/trunk/superskunk>
