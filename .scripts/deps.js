import {
  html,
  tokens,
} from "https://deno.land/x/rusty_markdown@v0.4.1/mod.ts";
export {
  CORE_SCHEMA as YAML_CORE_SCHEMA,
  parse,
} from "https://deno.land/std@0.120.0/encoding/yaml.ts";

/**
 * Composes the `html` and `tokens` methods from `rusty_markdown` into
 * one function for converting from Markdown to HTML.
 *
 * @param {string} markdown
 * @returns {string}
 */
export const md͜͡html = (markdown) => html(tokens(markdown));
