/**
 * Ensures a directory with the provided `name` exists by creating it
 * if it doesn’t.
 *
 * @param {string} name
 * @returns {Promise<unknown>}
 */
export default (name) => Deno.stat(name).catch(() => Deno.mkdir(name));
