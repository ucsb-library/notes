import { parse, YAML_CORE_SCHEMA } from "./deps.js";
import getFile from "./getFile.js";

/**
 * Gets the data in the `@.yml` (or similar) file in a directory or
 * returns an empty object if none is found.
 *
 * @param {string} directory
 * @returns {Promise<object>}
 */
export default async function dataFromFile(directory) {
  const dataSource = await getFile(directory, "@", [
    "json",
    "yaml",
    "yml",
  ]);
  return dataSource != null
    ? /** @type {object} */ (
      parse(dataSource.content, { schema: YAML_CORE_SCHEMA })
    )
    : {};
}
