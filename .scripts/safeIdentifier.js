/**
 * Ensures an identifier is safe both as an XML NMTOKEN and in a URL.
 *
 * Colons are disallowed because they are not permitted in `xml:id`.
 *
 * @param {string} string
 * @returns {string}
 */
export default (string) =>
  string.replace(/^~/u, "user-").replace(/^!/u, "project-").replaceAll(
    /[^A-Z_a-z\-\.0-9]/gu,
    "_",
  ) || "_";
