import safeIdentifier from "./safeIdentifier.js";

/**
 * @typedef {Object} TentativeDirectoryInfo
 * @property {?(string|string[])} [author]
 * @property {?string} [date]
 * @property {?string} [description]
 * @property {?string} [identifier]
 * @property {?string} [lang]
 * @property {?string} [stylesheet]
 * @property {string} title
 */

/**
 * @typedef {Object} DirectoryInfo
 * @property {string[]} author
 * @property {string} date
 * @property {string} dateTime
 * @property {?string} description
 * @property {string} identifier
 * @property {?string} lang
 * @property {string} title
 * @property {?string} stylesheet
 */

/**
 * Returns a safer, normalized form of the provided `info`.
 *
 * @param {TentativeDirectoryInfo} info
 * @returns {DirectoryInfo}
 */
export default (info) => {
  const {
    date,
    identifier,
    title,
    lang,
    stylesheet,
    author,
    description,
  } = info;
  const dateTime = (() => {
    try {
      return new Date(date ?? NaN).toISOString();
    } catch {
      // This is the XSD default date.
      return "1972-12-31T00:00:00.000Z";
    }
  })();
  return {
    author: /** @type {string[]} */ ([]).concat(author ?? []).map(
      (name) => name.trim(),
    ),
    date: /** @type {string[]} */ (
      // Just the date, not the time.
      /^[^T]*/.exec(dateTime)
    )[0],
    dateTime,
    description: description?.trim() ?? null,
    identifier: safeIdentifier(identifier?.trim() ?? title.trim()),
    lang: lang?.trim() ?? null,
    stylesheet: stylesheet?.trim() ?? null,
    title: title.trim(),
  };
};
