/**
 * Returns the first file in the provided `directory` with the
 * provided `name` and one of the provided `extensions`.
 *
 * @param {string | URL} directory
 * @param {string} name
 * @param {Iterable<string>} extensions
 * @param {{ ignoreContent?: boolean }} [options]
 * @returns {Promise<Deno.FileInfo & {content: string} | undefined>}
 */
export default async function getFile(
  directory,
  name,
  extensions,
  options,
) {
  const directoryPath = String(directory).replace(/\/?$/u, "/");
  return /** @type {{status: string, value: Deno.FileInfo & {content: string} | undefined }[]} */ (
    await Promise.allSettled(
      Array.from(extensions).map(
        (extension) => {
          const path = `${directoryPath}${name}.${extension}`;
          return Deno.stat(path).then((info) =>
            options?.ignoreContent
              ? { ...info, content: "" }
              : Deno.readTextFile(path).then((content) => ({
                ...info,
                content,
              }))
          );
        },
      ),
    )
  ).find(({ status }) => status == "fulfilled")?.value;
}
