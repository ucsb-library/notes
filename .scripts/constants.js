/**
 * XHTML used to display the page copyright in Markdown documents.
 */
export const copyrightXML = String.raw
  `<p>Copyright ©&#xA0;UC Regents. This page is licensed under a <a rel="license nofollow" href="https://creativecommons.org/licenses/by-nc/4.0/">Creative Commons BY‐NC&#xA0;4.0 license</a>.</p>`;

/**
 * Origin used for permanent entry identification.
 */
export const iriOrigin = "tag:ucsb-library.github.io,2022:notes";

/**
 * Origin at which this site will be hosted.
 */
export const origin = "https://ucsb-library.gitlab.io";

/**
 * Name of this repository, which forms the subpath at which this site
 * will be hosted.
 */
export const repositoryName = "notes";

/**
 * Directories with one of the following names will be copied verbatim
 * rather than processed.
 */
export const resourceDirectoryNames = Object.freeze([
  "media",
  "public",
  "resources",
  "scripts",
  "styles",
]);

/**
 * XHTML used to display the site name in Markdown documents.
 */
export const siteNameXML = String.raw
  `<small>UC&#xA0;<b>Santa Barbara</b></small> <strong>Digital Library Development</strong>`;
