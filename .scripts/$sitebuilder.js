#!/usr/bin/env -S deno run --config .scripts/deno.json --allow-read --allow-write --no-check
import {
  copyrightXML,
  iriOrigin,
  origin,
  repositoryName,
  resourceDirectoryNames,
  siteNameXML,
} from "./constants.js";
import dataFromFile from "./dataFromFile.js";
import ensureDirectoryExists from "./ensureDirectoryExists.js";
import escapeForXML from "./escapeForXML.js";
import generateIndex from "./generateIndex.js";
import normalizeInfo from "./normalizeInfo.js";

/**
 * Things to be moved verbatim.
 *
 * Includes all ordinary files and resource directories.
 *
 * @type {string[]}
 */
const movables = [];

/**
 * Directories to process.
 *
 * @type {string[]}
 */
const contentDirectories = [];

/**
 * Directories to generate redirects for.
 *
 * @type {string[]}
 */
const xhtmlDirectories = [];

/**
 * Info about the site as a whole.
 */
const siteData = normalizeInfo(
  Object.assign(
    { title: repositoryName },
    await dataFromFile(Deno.cwd()),
  ),
);

// Iterate over everything in the current working directory and
// determine whether it should be copied or processed.
for await (const { isDirectory, name } of Deno.readDir(Deno.cwd())) {
  if (
    !isDirectory && !/^[\._]/u.test(name) ||
    resourceDirectoryNames.includes(name)
  ) {
    movables.push(name);
  } else if (isDirectory && !/^[\._]/u.test(name)) {
    contentDirectories.push(name);
  }
}

// Remove and recreate the `.public-build` folder.
await Deno.remove(".public-build", { recursive: true }).finally(
  () => ensureDirectoryExists(".public-build"),
);

// Perform the following tasks asynchronously :—
await Promise.all([
  // Move the things wot can be straightforwardly moved into
  // `.public-build`.
  ...movables.map(
    (movable) => Deno.rename(movable, `.public-build/${movable}`),
  ),

  // Process each content directory.
  ...contentDirectories.map(
    async function (directory) {
      // Get directory data.
      const directoryData = normalizeInfo(
        Object.assign(
          { title: directory, identifier: directory },
          await dataFromFile(directory),
        ),
      );

      // Create the destination directiory.
      await ensureDirectoryExists(`.public-build/${directory}`);

      // Variable setup.
      /** @type {string[]} */
      const submovables = [];
      /** @type {string[]} */
      const notes = [];

      // Iterate over everything in the directory and determine
      // whether it should be copied, processed, or skipped.
      for await (
        const { isDirectory, name } of Deno.readDir(directory)
      ) {
        if (
          !isDirectory && !/^[\._]/u.test(name) ||
          resourceDirectoryNames.includes(name)
        ) {
          submovables.push(name);
        } else if (
          isDirectory && (
            /^\[(-?(?:[0-9]{4}|[1-9][0-9]{4,})(?:-(?:0[1-9]|1[0-2])(?:-(?:0[1-9]|[12][0-9]|3[01]))?)?)\]/u
          ).test(name)
        ) {
          notes.push(name);
        } else {
          continue;
        }
      }

      // Create note index files and remember note data.
      const noteDatas = Object.fromEntries(
        await Promise.all(
          notes.map(
            (note) =>
              generateIndex(`${directory}/${note}`, [
                {
                  name: siteData.title,
                  path: `/${repositoryName}/`,
                },
                {
                  name: directory,
                  path: `/${repositoryName}/${directory}/`,
                },
              ]).then((data) => [note, data]),
          ),
        ),
      );

      // Perform the following tasks asynchronously :—
      await Promise.all([
        // Move the things wot can be straightforwardly moved into
        // `.public-build`.
        ...submovables.map(
          (submovable) =>
            Deno.rename(
              `${directory}/${submovable}`,
              `.public-build/${directory}/${submovable}`,
            ),
        ),

        // Move notes into their correct location.
        ...Object.entries(noteDatas).map(
          ([note, { date, identifier }]) => {
            return ensureDirectoryExists(
              `.public-build/${directory}/${date}`,
            ).then(
              () =>
                Deno.rename(
                  `${directory}/${note}`,
                  `.public-build/${directory}/${date}/${identifier}`,
                ),
            );
          },
        ),

        // Create the Atom feed for the content directory.
        //
        // TODO: Actually it would be best if older entries were
        // put into an archive rather than sticking every single note
        // in the same feed.
        Deno.writeTextFile(
          `.public-build/${directory}/index.atom`,
          `<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="${
            directoryData.lang ?? "en"
          }">
  <id>${iriOrigin}/${directoryData.identifier}</id>
  <link rel="self" href="${origin}/${repositoryName}/${directory}/index.atom"/>
  <link rel="alternate" type="application/xhtml+xml" href="${origin}/${repositoryName}/${directory}/"/>
  <updated>${
            Object.values(noteDatas).map(
              ({ dateTime }) => dateTime,
            ).sort().pop()
          }</updated>
  <title>${escapeForXML(directoryData.title)}</title>
  ${
            directoryData.description != null
              ? String.raw`<subtitle>${
                escapeForXML(directoryData.description)
              }</subtitle>`
              : ""
          }
  ${
            (directoryData.author.length > 0
              ? directoryData.author
              : siteData.author).map(
                (name) =>
                  String.raw`<author><name>${
                    escapeForXML(name)
                  }</name></author>`,
              ).join("\n  ")
          }
  <rights type="xhtml">
    <div xmlns="http://www.w3.org/1999/xhtml">
      ${copyrightXML}
    </div>
  </rights>
  ${
            Object.values(noteDatas).map(
              (
                {
                  author,
                  date,
                  dateTime,
                  description,
                  identifier,
                  lang,
                  title,
                },
              ) =>
                String.raw
                  `<entry xml:id="${directoryData.identifier}.${date}.${identifier}" xml:lang="${
                    lang ?? "en"
                  }">
    <id>${iriOrigin}/${directoryData.identifier}/${date}/${identifier}</id>
    <link rel="alternate" type="application/xhtml+xml" href="${origin}/${repositoryName}/${directory}/${date}/${identifier}/"/>
    <updated>${dateTime}</updated>
    <title>${escapeForXML(title)}</title>
    ${
                    /** @type {string[]} */ (author).map(
                      (name) =>
                        String.raw`<author><name>${
                          escapeForXML(name)
                        }</name></author>`,
                    ).join("\n    ")
                  }
    <summary>${
                    escapeForXML(
                      description ??
                        "No description available.",
                    )
                  }</summary>
  </entry>`,
            ).join("\n  ")
          }
</feed>
`,
        ).catch(() => {/* do nothing */}),

        // Create an index file for the content directory if none
        // exists.
        Deno.stat(`${directory}/index.html`).catch(
          () => {
            xhtmlDirectories.push(directory);
            return Deno.stat(`${directory}/index.xhtml`);
          },
        ).catch(
          () =>
            Deno.writeTextFile(
              `.public-build/${directory}/index.xhtml`,
              `<?xml-stylesheet type="text/xsl" href="../resources/index.xslt"?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <title>${escapeForXML(directoryData.title)}</title>
  <link rel="alternate" type="application/atom+xml" href="index.atom"/>
</head>
<body>
  <article>
    <p>Viewing this page requires browser support for XSLT.</p>
  </article>
</body>
</html>`,
            ),
        ),
      ]);
    },
  ),

  // Create an index page if none exits.
  Deno.stat(`index.html`).catch(
    () =>
      Deno.writeTextFile(
        `.public-build/index.html`,
        `<!DOCTYPE html>
<html lang="${siteData.lang ?? "en"}">
<head>
  <title>${escapeForXML(siteData.title)}</title>
  <link rel="stylesheet" href="styles/plain.css"/>
</head>
<body>
  <header><div style="Display: Grid; Margin-Inline-End: Auto">${siteNameXML} <a href="/${repositoryName}/">${siteData.title}</a></div></header>
  <article>
    <header>
      <h1>
        ${escapeForXML(siteData.title)}
      </h1>
    </header>
    <div>
      ${escapeForXML(siteData.description ?? "")}
    </div>
    <nav>
      <h2 lang="en">Available Directories</h2>
      <ul class="PARAGRAPH">
        ${
          contentDirectories.sort().map((directory) =>
            String.raw`<li><a href="${directory}/">${directory}</a>`
          ).join("\n        ")
        }
      </ul>
    </nav>
  </article>
</body>
</html>`,
      ),
  ),
]);

// Create the `_redirects` file for XHTML directory indices.
await Deno.writeTextFile(
  `.public-build/_redirects`,
  xhtmlDirectories.map(
    (directory) =>
      `/${repositoryName}/${directory}/ /${repositoryName}/${directory}/index.xhtml 200
`,
  ).join(""),
);
