/**
 * Attempts to generate data regarding a file or directory from its
 * name.
 *
 * The accepted format is :—
 *
 *     Title of Document
 *     [1972-12-31] Title of Document
 *     [1972-12-31] identifier | Title of Document
 *     identifier | Title of Document
 *
 * @param {string} name
 * @returns {{date?: string, identifier?: string, title: string}}
 */
export default function dataFromName(name) {
  const groups =
    /** @type {{date?: string, identifier?: string, title: string}} */ (
      /^(?:\[(?<date>-?(?:[0-9]{4}|[1-9][0-9]{4,})(?:-(?:0[1-9]|1[0-2])(?:-(?:0[1-9]|[12][0-9]|3[01]))?)?)\][ \t]*)?(?:[ \t]*(?<identifier>[:A-Z_a-z][:A-Z_a-z\-\.0-9]*?)[ \t]*\|)?[ \t]*(?<title>.*?)[ \t]*(?:\.[0-9A-Za-z]+)?$/u
        .exec(name)?.groups ?? { title: name }
    );
  return { ...groups };
}
