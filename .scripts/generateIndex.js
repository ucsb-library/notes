import { md͜͡html } from "./deps.js";
import { copyrightXML, siteNameXML } from "./constants.js";
import dataFromFile from "./dataFromFile.js";
import dataFromName from "./dataFromName.js";
import escapeForXML from "./escapeForXML.js";
import getFile from "./getFile.js";
import normalizeInfo from "./normalizeInfo.js";

/**
 * Generates an index file in the provided `directory` if none already
 * exists.
 *
 * @param {string} directory
 * @param {{name: string, path: string}[]} ancestors
 * @returns {Promise<import("./normalizeInfo.js").DirectoryInfo>}
 */
export default async function generateIndex(directory, ancestors) {
  const directoryPath = directory.replace(/\/?$/u, "/");
  const data = normalizeInfo(
    Object.assign(
      dataFromName(
        /([^\/]*)\/$/u.exec(directoryPath)?.[1] ?? "",
      ),
      await dataFromFile(directory),
    ),
  );
  generatingIndex: {
    checkingForExistingIndex: {
      // Check for an existing index file, in which case do nothing.
      const existingIndex = await getFile(
        directory,
        "index",
        ["html"],
        { ignoreContent: true },
      );
      if (existingIndex != null) {
        break generatingIndex;
      } else {
        break checkingForExistingIndex;
      }
    }
    generatingIndexFromMarkdown: {
      // Generate an index file from a markdown source.
      const source = await getFile(directory, "index", [
        "markdown",
        "md",
      ]);
      if (source != null) {
        const mappedAncestors = ancestors.map(
          ({ name, path }) =>
            String.raw`<a href="${path}">${name}</a>`,
        );
        await Deno.writeTextFile(
          [directoryPath, "index.html"].join(""),
          String.raw`<!DOCTYPE html>
<html lang="${data.lang ?? "en"}">
<meta charset="utf-8">
<meta name="referrer" content="no-referrer">
<base target="_top">
<title>${data.title}</title>
<link rel="stylesheet" href="../../../${
            data.stylesheet ?? "styles/plain"
          }.css">
<header>
  <div style="Display: Grid; Margin-Inline-End: Auto">${siteNameXML} ${mappedAncestors.shift()}</div>
  <nav>${
            mappedAncestors.join("&MediumSpace;/&MediumSpace;")
          }&MediumSpace;/&MediumSpace;[${data.date}] <b style="Font-Style: Italic">${data.identifier}</b></nav>
</header>
<article id="content">

${md͜͡html(source.content)}
<footer>
  <p lang="en">Author${data.author.length != 1 ? "s" : ""}: ${
            data.author.length != 0
              ? data.author.map((name) => escapeForXML(name))
                .join(", ")
              : "<i>unknown</i>"
          }.</p>
  ${copyrightXML}
</footer>
</article>
`,
        );
        break generatingIndex;
      } else {
        break generatingIndexFromMarkdown;
      }
    }
  }
  return data;
}
