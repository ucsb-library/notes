/**
 * Escapes a string for insertion into an XML document.
 *
 * @param {string} string
 * @returns {string}
 */
export default (string) =>
  string.replaceAll(
    /[&<>'"]/gu,
    (char) => `&#x${char.charCodeAt(0).toString(16).toUpperCase()};`,
  );
