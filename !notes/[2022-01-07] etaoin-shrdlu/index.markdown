# <small><strong>thinking</strong> about:</small>The title of the page

A paragraph.

And here is <strong class="MARK">another</strong> <span class="PARENTHETICAL">(additional)</span> <i class="SO-CALLED">paragraph</i>.

## A subtitle

> This is a blockquote.
>
> > With a <mark>nested</mark> blockquote.

<figure><div style="border: thin solid">a figure</div><figcaption><p>with a caption</p></figcaption></figure>

<pre>some <strong>preformatted</strong> text
<code>with some code inside <small>/* comment */</small></code></pre>

<div><a href="#null" class="PREVIEW"><cite>preview link</cite><time>1972-12-31</time><p>a site preview link</p></a></div>

+ Unordered
+ List

1. Ordered
2. List

<ul class="PARAGRAPH"><li>paragraphy<li>list</ul>

<aside>an aside</aside>
